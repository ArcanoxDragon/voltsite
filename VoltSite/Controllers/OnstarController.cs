﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using VoltSite.Models;
using VoltSite.Services;

namespace VoltSite.Controllers
{
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class OnstarController : Controller
	{
		private readonly ILogger<OnstarController> logger;
		private readonly SessionManager            sessionManager;

		public OnstarController( ILogger<OnstarController> logger, SessionManager sessionManager )
		{
			this.logger         = logger;
			this.sessionManager = sessionManager;
		}

		protected SessionData Session { get; private set; }

		public override async Task OnActionExecutionAsync( ActionExecutingContext context, ActionExecutionDelegate next )
		{
			var session = await this.sessionManager.GetCurrentSessionAsync();

			if ( session == null )
			{
				context.Result = BadRequest();
				return;
			}

			this.Session = session;

			await next();
		}

		public IActionResult GetStats()
		{
			var context = this.sessionManager.GetContext( this.Session );
			var model   = new GetStatsModel();

			if ( context.DataManager.HasData )
				model.Data = context.DataManager.CurrentData;

			if ( context.DataManager.DidError )
				model.Error = context.DataManager.LastException.Message;

			if ( context.DataManager.IsPollingRequested || context.DataManager.CurrentlyPolling )
				model.IsPolling = true;

			return Json( model );
		}

		[ HttpPost ]
		public IActionResult RefreshStats()
		{
			var context = this.sessionManager.GetContext( this.Session );

			context.DataManager.RequestPolling( this.Session.Username, this.Session.Password );

			return Ok();
		}
	}
}