﻿using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VoltSite.Models;
using VoltSite.Services;

namespace VoltSite.Controllers
{
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class HomeController : Controller
	{
		private readonly SessionManager sessionManager;

		public HomeController( SessionManager sessionManager )
		{
			this.sessionManager = sessionManager;
		}

		[ HttpGet( "/login" ) ]
		public async Task<IActionResult> Login()
		{
			var session = await this.sessionManager.GetCurrentSessionAsync();

			if ( !string.IsNullOrEmpty( session?.Username ) && !string.IsNullOrEmpty( session?.Password ) )
				return RedirectToAction( "Index" );

			var model = new LoginViewModel();

			if ( Request.Cookies.TryGetValue( "Username", out var username ) )
				model.Username = username;

			return View( model );
		}

		[ HttpPost( "/login" ) ]
		public async Task<IActionResult> Login( LoginViewModel model )
		{
			Response.Cookies.Append( "Username", model.Username );

			if ( !ModelState.IsValid )
				return View( model );

			try
			{
				var (success, _) = await this.sessionManager.CreateSessionAsync( model.Username, model.Password );

				if ( success )
					return RedirectToAction( "Index" );
			}
			catch
			{
				// Ignored
			}

			ModelState.AddModelError( "", "Unable to start session with OnStar" );

			return View( model );
		}

		[ HttpPost ]
		public async Task<IActionResult> Logout()
		{
			HttpContext.Session.Clear();

			await this.HttpContext.Session.CommitAsync( HttpContext.RequestAborted );

			return RedirectToAction( "Login" );
		}

		public async Task<IActionResult> Index()
		{
			var session = await this.sessionManager.GetCurrentSessionAsync();

			if ( string.IsNullOrEmpty( session?.Username ) || string.IsNullOrEmpty( session?.Password ) )
				return RedirectToAction( "Login" );

			await this.sessionManager.KeepAliveAsync( session );

			return View();
		}

		[ ResponseCache( Duration = 0, Location = ResponseCacheLocation.None, NoStore = true ) ]
		public IActionResult Error() => View( new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier } );
	}
}