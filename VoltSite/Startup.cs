﻿using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.React.Options;
using Arcanox.AspNetCoreHelpers.Features.Resources.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyChevrolet.Services;
using VoltSite.HostedServices;
using VoltSite.Services;

namespace VoltSite
{
	public class Startup
	{
		public Startup( IConfiguration configuration )
		{
			this.Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices( IServiceCollection services )
		{
			services.AddRouting( options => options.LowercaseUrls = true );
			services.AddMvc()
					.SetCompatibilityVersion( CompatibilityVersion.Version_2_2 );
			services.AddDistributedMemoryCache();
			services.AddSession( options => options.IdleTimeout = SessionManager.SessionLifetime );
			services.AddHttpContextAccessor();

			// Find dynamic services
			services.FindAndAddServices();

			// Configure options
			services.Configure<ResourceOptions>( options => {
				options.PageScriptBaseUrl = "/bundle/js";
				options.PageStyleBaseUrl  = "/bundle/css";
			} );
			services.Configure<ReactOptions>( options => {
				options.ReactEntryPointFile = "/bundle/js/react-mount.js";
			} );

			// Configure MyChevrolet.NET services
			services.AddTransient<OnStarService>();

			// Configure hosted services
			services.AddHostedService<WorkerService>();
		}

		public void Configure( IApplicationBuilder app, IHostingEnvironment env )
		{
			if ( env.IsDevelopment() )
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler( "/Home/Error" );
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseCookiePolicy();
			app.UseSession();
			app.UseMvc( routes => {
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}" );
			} );
		}
	}
}