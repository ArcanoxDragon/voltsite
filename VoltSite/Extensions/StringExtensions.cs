﻿using System;
using System.Linq;
using Arcanox.AspNetCoreHelpers.Extensions;

namespace VoltSite.Extensions
{
	public static class StringExtensions
	{
		public static string Obfuscate( this string s, params char[] delimiters )
		{
			if ( delimiters.Length == 0 )
			{
				if ( s.Length < 2 )
					return s;

				return s.First() + "*".Repeat( Math.Max( 0, s.Length - 2 ) ) + s.Last();
			}

			foreach ( var delimiter in delimiters )
			{
				s = string.Join( delimiter.ToString(), s.Split( delimiter ).Select( part => part.Obfuscate() ) );
			}

			return s;
		}
	}
}