import React from "react";

import { EvStats, GetStatsResponse } from "models";
import { BindMethod } from "@arcanox/asp-net-core-helpers";
import { isThisTypeNode } from "typescript";

interface State {
    loading: boolean;
    data: EvStats;
    error: string;
}

export default class IndexApp extends React.Component<{}, State> {
    state: State = {
        loading: true,
        data: null,
        error: null,
    };

    componentDidMount() {
        this.loadData();
    }

    @BindMethod
    private loadData() {
        this.setState( { loading: true, error: null } );

        $.ajax( {
            url: "/onstar/getstats",
            error: this.onError,
            success: this.onDataLoaded,
        } );
    }

    @BindMethod
    private refreshData() {
        this.setState( { loading: true } );

        $.ajax( {
            method: "POST",
            url: "/onstar/refreshstats",
            error: this.onError,
            success: () => setTimeout( this.loadData, 1000 ),
        } );
    }

    @BindMethod
    private onError( xhr: JQueryXHR, status: string, errorThrown: string ) {
        this.setState( { loading: false } );

        if ( xhr && xhr.responseText ) {
            this.setState( { error: xhr.responseText } );
        } else if ( errorThrown ) {
            this.setState( { error: errorThrown } );
        } else {
            this.setState( { error: "Unknown Error" } );
        }
    }

    @BindMethod
    private onDataLoaded( response: GetStatsResponse ) {
        if ( !response ) {
            this.setState( { loading: false, error: "Unknown Error" } );
            return;
        }

        this.setState( { loading: response.isPolling } );

        if ( response.hasError ) {
            this.setState( { error: response.error } );
        }

        if ( response.hasData ) {
            this.setState( { data: response.data } );
        }

        if ( response.isPolling ) {
            setTimeout( this.loadData, 1000 );
        }
    }

    renderBody() {
        let { data, error } = this.state;

        if ( error ) {
            return <div>Error: { error }</div>;
        } else if ( !data ) {
            return <div>No data. Click the "Refresh" button.</div>;
        }

        let {
            batteryLevel,
            chargeState,
            plugState,
            estimatedFullChargeBy,
            electricRange,
            electricRangeUOM,
            gasRange,
            gasRangeUOM,
            totalRange,
            totalRangeUOM,
            voltage
        } = data;

        return <table className="stats-wrapper">
            <tbody>
                <tr>
                    <td className="table-label">Plugged In</td>
                    <td>{ plugState === "plugged" ? `Yes @ ${voltage}V` : "No" }</td>
                </tr>
                <tr>
                    <td className="table-label">Charging</td>
                    <td>{ chargeState === "charging_complete" ? "Complete" : ( chargeState === "charging" ? "Yes" : "No" ) }</td>
                </tr>
                { chargeState === "charging" && <tr>
                    <td className="table-label">Complete By</td>
                    <td>{ estimatedFullChargeBy }</td>
                </tr> }
                <tr>
                    <td className="table-label">Battery Level</td>
                    <td>{ Math.round( batteryLevel || 0 ) }%</td>
                </tr>
                <tr>
                    <td className="table-label">EV Range</td>
                    <td>{ electricRange } { electricRangeUOM }</td>
                </tr>
                <tr>
                    <td className="table-label">Gas Range</td>
                    <td>{ gasRange } { gasRangeUOM }</td>
                </tr>
                <tr>
                    <td className="table-label">Total Range</td>
                    <td>{ totalRange } { totalRangeUOM }</td>
                </tr>
            </tbody>
        </table>;
    }

    render() {
        let { loading } = this.state;

        return <div className="main-wrapper">
            { this.renderBody() }
            <button className="refresh-button btn btn-lg btn-success" onClick={ this.refreshData } disabled={ loading }>
                { loading ? "Loading..." : "Refresh" }
            </button>
        </div>;
    }
}