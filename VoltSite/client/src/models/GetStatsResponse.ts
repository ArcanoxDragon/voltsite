import { EvStats } from "models/EvStats";

export interface GetStatsResponse {
    isPolling: boolean;
    data: EvStats;
    error: string;
    
    hasData: boolean;
    hasError: boolean;
}