export interface EvStats {
    dataAsOfDate: Date;
    batteryLevel: number;
    chargeState: "not_charging" | "charging" | "charging_complete";
    chargeMode: string;
    plugState: "plugged" | "unplugged";
    rateType: string;
    voltage: number;
    electricRange: number;
    gasRange: number;
    totalRange: number;
    fuelEconomy: number;
    fuelUsed: number;
    estimatedFullChargeBy: string;
    gasFuelLevelPercentage: string;

    electricRangeUOM: "mi" | "km";
    gasRangeUOM: "mi" | "km";
    totalRangeUOM: "mi" | "km";
    fuelEconomyUOM: string;
    fuelUsedUOM: string;
}