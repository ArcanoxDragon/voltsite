$( () => {
    var $username = $( "#Username" );
    var $password = $( "#Password" );

    if ( $username.val().toString().trim().length === 0 ) {
        $username.focus();
    } else {
        $password.focus();
    }
} );