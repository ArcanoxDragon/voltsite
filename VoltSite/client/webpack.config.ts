import path from "path";
import webpack from "webpack";
import findScriptEntryPoints from "./build/lib/findScriptEntryPoints";
import findStyleEntryPoints from "./build/lib/findStyleEntryPoints";

import { TsConfigPathsPlugin } from "awesome-typescript-loader";
import { loader as MiniCssExtractLoader } from "mini-css-extract-plugin";

import CleanPlugin from "clean-webpack-plugin";
import HardSourcePlugin from "hard-source-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import TerserPlugin from "terser-webpack-plugin";

interface Environment {
    prod: boolean;
}

const DefaultEnvironment: Environment = {
    prod: false,
};

export default function buildConfiguration( env: Environment ): webpack.Configuration {
    const { prod } = Object.assign( DefaultEnvironment, env );
    const mode = prod ? "production" : "development";
    const sourceMap = !prod;
    const outputPath = path.join( __dirname, "../wwwroot/bundle" );

    return {
        mode,
        devtool: sourceMap ? "inline-source-map" : false,

        entry: {
            ...findScriptEntryPoints(),
            ...findStyleEntryPoints(),
        },

        module: {
            rules: [
                {
                    test: /\.tsx?$/i,
                    use: {
                        loader: "awesome-typescript-loader",
                        options: {
                            useBabel: prod,
                            babelCore: "@babel/core",
                            useCache: true,
                            useTranspileModule: !prod,
                            reportFiles: [
                                "src/**/*.{ts,tsx}"
                            ],
                            compilerOptions: {
                                sourceMap,
                            }
                        },
                    },
                },
                {
                    test: /\.scss$/i,
                    use: [
                        MiniCssExtractLoader,
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: { sourceMap },
                        },
                    ],
                },
            ]
        },

        optimization: {
            namedModules: true,
            minimize: prod,
            minimizer: [ new TerserPlugin( {
                exclude: /node_modules\//i
            } ) ],
            runtimeChunk: {
                name: "js/manifest"
            },
            splitChunks: {
                name( module: webpack.compilation.Module, chunks: webpack.compilation.Chunk[], cacheGroupKey: string ) {
                    function getCleanChunkName( chunk: webpack.compilation.Chunk ) {
                        const nameMatch = chunk.name.match( /.*?([^/]+)$/i );

                        if ( !nameMatch )
                            throw new Error( "Invalid chunk name: " + chunk.name );

                        return nameMatch[ 1 ];
                    }

                    return "js/chunks/" + chunks.map( getCleanChunkName ).join( "~" );
                },

                cacheGroups: {
                    vendor: {
                        name: "js/vendor",
                        chunks: "initial",
                        test: /[\\/]node_modules[\\/]/i,
                        priority: -10
                    },
                    common: {
                        name: "js/common",
                        chunks: "all",
                        minChunks: 2,
                        priority: -20,
                        reuseExistingChunk: true
                    },
                }
            }
        },

        output: {
            path: outputPath,
            publicPath: "/bundle/",
            filename: "[name].js",
            chunkFilename: "[name].js?v=[chunkhash]",
        },

        plugins: [
            new CleanPlugin( {
                cleanOnceBeforeBuildPatterns: [ "js" ],
                cleanAfterEveryBuildPatterns: [ "css/**/*.{js,js.map}" ],
            } ),

            new MiniCssExtractPlugin( {
                filename: "[name].css",
                chunkFilename: "[name].css?v=[chunkhash]",
            } ),

            new HardSourcePlugin(),
        ],

        resolve: {
            extensions: [ ".js", ".ts", ".tsx" ],
            plugins: [ new TsConfigPathsPlugin() ],
        },
    };
}