﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MyChevrolet.Services;
using VoltSite.Models;
using VoltSite.Services;

namespace VoltSite.HostedServices
{
	public class WorkerService : IHostedService, IDisposable
	{
		private readonly ILogger<WorkerService> logger;
		private readonly OnStarService          onStar;
		private readonly SessionManager         sessionManager;

		private CancellationTokenSource workTokenSource;
		private Task                    workingTask;

		public WorkerService( ILogger<WorkerService> logger, OnStarService onStar, SessionManager sessionManager )
		{
			this.logger         = logger;
			this.onStar         = onStar;
			this.sessionManager = sessionManager;
		}

		public Task StartAsync( CancellationToken cancellationToken )
		{
			this.logger.LogDebug( "Starting worker service..." );

			this.workTokenSource = new CancellationTokenSource();
			this.workingTask     = Task.Run( this.DoWorkAsync );

			this.logger.LogDebug( "Worker service started." );

			return Task.CompletedTask;
		}

		public async Task StopAsync( CancellationToken cancellationToken )
		{
			if ( this.workingTask == null )
			{
				this.logger.LogDebug( "Worker service was asked to stop, but it wasn't running." );
				return;
			}

			this.logger.LogDebug( "Stopping worker service..." );

			try
			{
				this.workTokenSource?.Cancel();
			}
			finally
			{
				await Task.WhenAny( this.workingTask, Task.Delay( Timeout.Infinite, cancellationToken ) );
			}
		}

		private async Task DoWorkAsync()
		{
			this.logger.LogDebug( "[WT] Worker thread starting up..." );

			while ( !this.workTokenSource.IsCancellationRequested )
			{
				this.sessionManager.PurgeExpiredSessions();

				foreach ( var session in this.sessionManager.Sessions )
				{
					if ( session.DataManager.IsPollingRequested )
					{
						this.logger.LogDebug( $"[WT] Polling is requested for session {session.SessionId:D}. Starting session worker." );

						Task.Run( () => this.DoWorkForSessionAsync( session ) ).Forget();
					}

					session.DataManager.CleanupIfOutdated();
				}

				await Task.Delay( 5000 );
			}

			this.logger.LogDebug( "[WT] Worker thread shutting down..." );
		}

		private async Task DoWorkForSessionAsync( SessionContext session )
		{
			var ( username, password ) = session.DataManager.HandlePollingRequest();

			this.logger.LogDebug( $"[ST] Starting work for session {session.SessionId:D}" );

			try
			{
				var onStarSession = await this.onStar.StartSessionAsync( username, password );

				if ( onStarSession.VehicleMap.Values.Count != 1 )
					throw new Exception( "Only one vehicle supported" );

				var stats = await this.onStar.GetEvStatsAsync( onStarSession, onStarSession.VehicleMap.Values.Single() );

				if ( session.Disposed )
					return;

				session.DataManager.NotifyPollingCompleted( stats );
			}
			catch ( Exception ex )
			{
				session.DataManager.NotifyPollingFailed( ex );
			}

			this.logger.LogDebug( $"[ST] Work finished for session {session.SessionId:D}." );
		}

		public void Dispose()
		{
			this.workTokenSource?.Cancel();
		}
	}
}