﻿using System;
using Microsoft.Extensions.Logging;
using MyChevrolet.Models.Responses;
using VoltSite.Extensions;

namespace VoltSite.Services
{
	public class DataManager
	{
		private static readonly TimeSpan DataLifespan = TimeSpan.FromMinutes( 5 );

		private readonly ILogger<DataManager> logger;

		public DataManager( ILogger<DataManager> logger )
		{
			this.logger = logger;
		}

		public bool        IsPollingRequested { get; private set; }
		public bool        CurrentlyPolling   { get; private set; }
		public EvStatsData CurrentData        { get; private set; }
		public Exception   LastException      { get; private set; }
		public DateTime    LastGotData        { get; private set; }

		private string Username { get; set; }
		private string Password { get; set; }

		public bool DidError => this.LastException != null;
		public bool HasData  => this.CurrentData != null;

		public void CleanupIfOutdated()
		{
			if ( this.HasData && DateTime.Now > this.LastGotData + DataLifespan )
			{
				this.CurrentData = null;
			}
		}

		public void RequestPolling( string username, string password )
		{
			if ( this.CurrentlyPolling )
				return;

			this.logger.LogDebug( $"Polling requested for OnStar account {username.Obfuscate( '@' )}." );

			this.Username           = username;
			this.Password           = password;
			this.IsPollingRequested = true;
		}

		public (string username, string password) HandlePollingRequest()
		{
			var username = this.Username;
			var password = this.Password;

			this.IsPollingRequested = false;
			this.CurrentlyPolling   = true;

			this.logger.LogDebug( $"Polling request claimed for OnStar account {username.Obfuscate( '@' )}." );

			return ( username, password );
		}

		public void NotifyPollingCompleted( EvStatsData stats )
		{
			this.logger.LogDebug( $"Polling completed for OnStar account {this.Username.Obfuscate( '@' )}." );

			this.CurrentlyPolling = false;
			this.CurrentData      = stats;
			this.LastException    = null;
			this.LastGotData      = DateTime.Now;
			this.Username         = null;
			this.Password         = null;
		}

		public void NotifyPollingFailed( Exception ex )
		{
			this.logger.LogError( ex, $"Polling failed for OnStar account {this.Username.Obfuscate( '@' )}." );

			this.CurrentlyPolling = false;
			this.LastException    = ex;
			this.Username         = null;
			this.Password         = null;
		}
	}
}