﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Attributes;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MyChevrolet.Services;
using Newtonsoft.Json;
using VoltSite.Models;

namespace VoltSite.Services
{
	[ Singleton ]
	public class SessionManager
	{
		public static readonly TimeSpan SessionLifetime = TimeSpan.FromMinutes( 15 );

		public const string SessionDataKey = "SessionData";

		private readonly ILogger<SessionManager>          logger;
		private readonly OnStarService                    onStar;
		private readonly IHttpContextAccessor             httpContextAccessor;
		private readonly DataManagerFactory               dataManagerFactory;
		private readonly Dictionary<Guid, SessionContext> sessions;

		public SessionManager( ILogger<SessionManager> logger,
							   OnStarService onStar,
							   IHttpContextAccessor httpContextAccessor,
							   DataManagerFactory dataManagerFactory )
		{
			this.logger              = logger;
			this.onStar              = onStar;
			this.httpContextAccessor = httpContextAccessor;
			this.dataManagerFactory  = dataManagerFactory;
			this.sessions            = new Dictionary<Guid, SessionContext>();
		}

		public IEnumerable<SessionContext> Sessions => this.sessions.Values.ToList();

		public async Task<(bool success, SessionData session)> CreateSessionAsync( string username, string password )
		{
			try
			{
				var sessionGuid = Guid.NewGuid();

				this.logger.LogDebug( $"Starting session {sessionGuid:D}..." );

				var gmSession = await this.onStar.StartSessionAsync( username, password );
				var sessionData = new SessionData {
					Id         = sessionGuid,
					Username   = username,
					Password   = password,
					GmSession  = gmSession,
					LastActive = DateTime.UtcNow,
				};

				this.logger.LogDebug( $"Session {sessionGuid:D} started. Account has {gmSession.VehicleMap.Count} vehicles." );

				await this.PersistSessionIfNeededAsync( sessionData );

				return ( true, sessionData );
			}
			catch
			{
				return ( false, null );
			}
		}

		public async Task<SessionData> GetCurrentSessionAsync()
		{
			var httpSession = this.httpContextAccessor.HttpContext?.Session;

			if ( httpSession == null )
				return null;

			await httpSession.LoadAsync( this.httpContextAccessor.HttpContext.RequestAborted );

			var sessionJson = httpSession.GetString( SessionDataKey );

			if ( string.IsNullOrEmpty( sessionJson ) )
				return null;

			try
			{
				var sessionData = JsonConvert.DeserializeObject<SessionData>( sessionJson );

				if ( sessionData == null )
					return null;

				if ( sessionData.LastActive < DateTime.UtcNow - SessionLifetime )
				{
					if ( string.IsNullOrEmpty( sessionData.Username ) || string.IsNullOrEmpty( sessionData.Password ) )
					{
						return null;
					}

					try
					{
						this.logger.LogDebug( $"Session {sessionData.Id:D} expired; renewing session with OnStar..." );

						var gmSession = await this.onStar.StartSessionAsync( sessionData.Username, sessionData.Password );

						this.logger.LogDebug( $"Session {sessionData.Id:D} successfully renewed with OnStar." );

						sessionData.GmSession = gmSession;

						if ( this.sessions.ContainsKey( sessionData.Id ) )
							this.sessions.Remove( sessionData.Id );
					}
					catch
					{
						return null;
					}
				}

				this.logger.LogDebug( $"Successfully loaded session {sessionData.Id:D}." );

				await this.PersistSessionIfNeededAsync( sessionData );

				return sessionData;
			}
			catch ( JsonException )
			{
				return null;
			}
		}

		public SessionContext GetContext( SessionData session )
		{
			if ( !this.sessions.TryGetValue( session.Id, out var context ) )
				return null;

			return context;
		}

		public async Task KeepAliveAsync( SessionData session )
		{
			if ( !this.sessions.ContainsKey( session.Id ) )
				return;

			session.LastActive = DateTime.UtcNow;

			await this.PersistSessionIfNeededAsync( session );
		}

		public void PurgeExpiredSessions()
		{
			var toPurge = this.sessions.Values.Where( s => s.LastActive < DateTime.UtcNow - SessionLifetime ).ToList();

			if ( toPurge.Any() )
				this.logger.LogDebug( $"Purging {toPurge.Count} expired sessions..." );

			foreach ( var session in toPurge )
			{
				if ( !this.sessions.ContainsKey( session.SessionId ) )
					// WTF tho
					continue;

				session.DataManager.CleanupIfOutdated();
				session.Disposed = true;

				this.sessions.Remove( session.SessionId );
			}
		}

		private async Task PersistSessionIfNeededAsync( SessionData session )
		{
			var httpSession = this.httpContextAccessor.HttpContext?.Session;

			if ( httpSession != null )
			{
				var sessionJson = JsonConvert.SerializeObject( session );

				httpSession.SetString( SessionDataKey, sessionJson );

				await httpSession.CommitAsync( this.httpContextAccessor.HttpContext.RequestAborted );
			}

			if ( this.sessions.ContainsKey( session.Id ) )
				return;

			var sessionContex = new SessionContext {
				SessionId   = session.Id,
				LastActive  = session.LastActive,
				DataManager = this.dataManagerFactory.CreateDataManager(),
			};

			this.sessions[ session.Id ] = sessionContex;
		}
	}
}