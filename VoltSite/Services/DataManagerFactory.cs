﻿using System;
using Arcanox.AspNetCoreHelpers.Attributes;
using Microsoft.Extensions.Logging;

namespace VoltSite.Services
{
	[ Transient ]
	public class DataManagerFactory
	{
		private readonly ILoggerFactory   loggerFactory;
		private readonly IServiceProvider services;

		public DataManagerFactory( ILoggerFactory loggerFactory, IServiceProvider services )
		{
			this.loggerFactory = loggerFactory;
			this.services      = services;
		}

		public DataManager CreateDataManager()
		{
			var logger = this.loggerFactory.CreateLogger<DataManager>();

			return new DataManager( logger );
		}
	}
}