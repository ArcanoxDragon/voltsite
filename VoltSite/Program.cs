﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace VoltSite
{
	public class Program
	{
		public static void Main( string[] args )
		{
			CreateWebHostBuilder( args ).Build().Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder( string[] args ) =>
			WebHost.CreateDefaultBuilder( args )
				   .ConfigureLogging( logging => {
					   logging.AddFilter( "Microsoft", LogLevel.Warning );
					   logging.AddFilter( "System", LogLevel.Warning );
					   logging.AddFilter( nameof(VoltSite), LogLevel.Debug );
					   logging.AddConsole();
				   } )
				   .UseStartup<Startup>();
	}
}