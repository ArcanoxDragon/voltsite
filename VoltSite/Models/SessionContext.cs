﻿using System;
using VoltSite.Services;

namespace VoltSite.Models
{
	public class SessionContext
	{
		public Guid        SessionId   { get; set; }
		public DataManager DataManager { get; set; }
		public DateTime    LastActive  { get; set; }
		public bool        Disposed    { get; set; }
	}
}