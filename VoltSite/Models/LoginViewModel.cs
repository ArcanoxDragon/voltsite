﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VoltSite.Models
{
	public class LoginViewModel
	{
		[ Required ]
		[ DisplayName( "OnStar E-mail Address:" ) ]
		public string Username { get; set; }

		[ Required ]
		[ DisplayName( "Password:" ) ]
		public string Password { get; set; }
	}
}