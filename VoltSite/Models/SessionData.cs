﻿using System;
using MyChevrolet;

namespace VoltSite.Models
{
	public class SessionData
	{
		public Guid      Id         { get; set; }
		public string    Username   { get; set; }
		public string    Password   { get; set; }
		public GmSession GmSession  { get; set; }
		public DateTime  LastActive { get; set; }
	}
}