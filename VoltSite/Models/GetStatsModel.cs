﻿using MyChevrolet.Models.Responses;

namespace VoltSite.Models
{
	public class GetStatsModel
	{
		public bool        IsPolling { get; set; }
		public EvStatsData Data      { get; set; }
		public string      Error     { get; set; }

		public bool HasData  => this.Data != null;
		public bool HasError => this.Error != null;
	}
}